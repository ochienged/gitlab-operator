package runner

const (
	// GitlabRunnerImage represents the runner image
	GitlabRunnerImage = "registry.gitlab.com/gitlab-org/gl-openshift/gitlab-runner-ubi:latest"

	// CertifiedRunnerImage represents the runner image
	CertifiedRunnerImage = "registry.connect.redhat.com/gitlab/gitlab-runner:12.9"

	// GitlabRunnerHelperImage represents the runner image
	GitlabRunnerHelperImage = "registry.gitlab.com/gitlab-org/gl-openshift/gitlab-operator/gitlab-runner-helper"
)
