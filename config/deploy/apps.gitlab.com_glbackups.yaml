apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  annotations:
    cert-manager.io/inject-ca-from: gitlab-system/gitlab-serving-cert
    controller-gen.kubebuilder.io/version: v0.3.0
  creationTimestamp: null
  name: glbackups.apps.gitlab.com
spec:
  conversion:
    strategy: Webhook
    webhookClientConfig:
      caBundle: Cg==
      service:
        name: gitlab-webhook-service
        namespace: gitlab-system
        path: /convert
        port: 443
  group: apps.gitlab.com
  names:
    kind: GLBackup
    listKind: GLBackupList
    plural: glbackups
    shortNames:
    - gbk
    singular: glbackup
  preserveUnknownFields: false
  scope: Namespaced
  subresources:
    status: {}
  validation:
    openAPIV3Schema:
      description: GLBackup resource backups and restores a GitLab instance
      properties:
        apiVersion:
          description: 'APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
          type: string
        kind:
          description: 'Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
          type: string
        metadata:
          type: object
        spec:
          description: Specification of the desired behavior of a GitLab Backup
          properties:
            instance:
              description: Name of GitLab instance to backup
              type: string
            restore:
              description: If set to true, informs GitLab operator to perform a backup restore. Defaults to false or performing a backup.
              type: boolean
            schedule:
              description: Backup schedule in cron format. Leave blank for one time on-demand backup
              type: string
            skip:
              description: Comma-separated list of components to omit from backup.
              type: string
            timestamp:
              description: If specified, overrides the timestamp of a backup. Forms the prefix of the backup e.g. '<timestamp-override-value>_gitlab_backup.tar'. Can also be specified to target a specific backup to be restored
              type: string
            url:
              description: The URL of the backup resource to be restored
              type: string
          required:
          - instance
          type: object
        status:
          description: Most recently observed status of the GitLab Backup. It is read-only to the user
          properties:
            completedAt:
              description: Displays time the backup completed
              type: string
            phase:
              description: Reports status of backup job
              enum:
              - Running
              - Completed
              - Scheduled
              - Failed
              type: string
            startedAt:
              description: Displays time the backup started
              type: string
          type: object
      type: object
  version: v1beta1
  versions:
  - name: v1beta1
    served: true
    storage: true
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []
